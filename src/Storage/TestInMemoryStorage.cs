﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using src.Data;

namespace src.Storage
{
    public class TestInMemoryStorage : IStorage
    {
        private readonly Dictionary<string, WordEntry> _wordsDictionary;

        public TestInMemoryStorage()
        {
            _wordsDictionary = new Dictionary<string, WordEntry>();
        }

        public IEnumerable<WordEntry> GetAll() => _wordsDictionary.Values.OrderBy(x => x.Count);

        public void PushWords(IEnumerable<SubtitleItem> subs)
        {
            foreach (var sub in subs)
            {
                var cleanStr = Regex.Replace(sub.Text, "[^A-Za-z0-9 -']", "")
                    .Replace("!", ""); // ?

                var arr = cleanStr.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                foreach (var word in arr)
                {
                    if (string.IsNullOrEmpty(word))
                        continue;

                    if (_wordsDictionary.Keys.Contains(word))
                        _wordsDictionary[word].Increment();
                    else
                        _wordsDictionary.Add(word, new WordEntry(word));

                    _wordsDictionary[word].FoundInSub(sub.Num);
                }
            }
        }
    }
}