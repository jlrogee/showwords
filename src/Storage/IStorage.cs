using System.Collections.Generic;
using src.Data;

namespace src.Storage
{
     public interface IStorage
     {
        void PushWords(IEnumerable<SubtitleItem> subs);
        IEnumerable<WordEntry> GetAll();
     }
}