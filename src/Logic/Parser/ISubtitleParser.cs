﻿using System.Collections.Generic;
using src.Data;

namespace src.Logic.Parser
{
    public interface ISubtitleParser
    {
        IEnumerable<SubtitleItem> Parse(string src);
    }
}
