namespace src.Services
{
    public interface ISessionHashProvider
    {
        string GetHash(object payloadFile);
    }
}