﻿using System;
namespace src.Data
{
    public class SubtitleItem
    {
        public SubtitleItem()
        {
        }

        public int Num { get; set; }
        public int Start { get; set; }
        public int End { get; set; }
        public string Text { get; set; }
    }
}
