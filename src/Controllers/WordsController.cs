using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using src.Data;
using src.Logic.Parser;
using src.Storage;

namespace src.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WordsController : ControllerBase
    {
        private readonly ISubtitleParser _subtitleParser;
        private readonly IStorage _storage;
        
        public WordsController(ISubtitleParser subtitleParser, IStorage storage)
        {
            _subtitleParser = subtitleParser;
            _storage = storage;
        }

        /// <summary>
        /// Get all words for current session
        /// </summary>
        [HttpGet]
        public ActionResult GetAll()
        {
            // todo в редис с учетом текущего пользовтеля и ео сессии≠
            return Ok(_storage.GetAll()); 
        }
        
        /// <summary>
        /// Upload subtitles to storage
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Post(IFormFile file)
        {

            if (file.Length == 0 || string.IsNullOrEmpty(file.FileName))
                return BadRequest("File is empty");

            string fileContent;

            using (var stream = new StreamReader(file.OpenReadStream()))
            {
                fileContent = await stream.ReadToEndAsync();
            }
            
            if (string.IsNullOrEmpty(fileContent))
                return BadRequest("Empty text");
            
            var subs = _subtitleParser.Parse(fileContent);

            var subtitleItems = subs as SubtitleItem[] ?? subs.ToArray();
            if (!subtitleItems.Any())
                return Ok("No subs found");
            
            _storage.PushWords(subtitleItems);
            return Ok();
        }
        
        /// <summary>
        /// Need word to ignore
        /// </summary>
        [HttpGet]
        [Route("AlreadyKnown")]
        public IActionResult AlreadyKnown(string word)
        {
            return Ok(false);
        }
    }
}
